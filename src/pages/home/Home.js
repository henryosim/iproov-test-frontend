import "./Home.css";
import { useEffect, useRef, useState } from "react";
import * as faceApi from "face-api.js";
import { processMedia } from "../../components/api/mediaApi";
import { useAuth } from "../../contexts/AuthContext";
import { getUserData, login } from "../../components/api/userApi";

function Home() {
  const STATE_READY = "Ready";
  const TIME_INTERVAL = 100; //milliseconds
  const READY_TIME = 2000; // 2secons
  const [status, setStatus] = useState(null);
  const [isProcessingImage, setIsProcessingImage] = useState(false);
  const [isStreamingVideo, setIsStreamingVideo] = useState(false);
  const [processedImageUrl, setProcessedImageUrl] = useState(null);
  const MODEL_URL = process.env.PUBLIC_URL + "/models";
  const videoWidth = 640;
  const videoHeight = 480;
  const videoRef = useRef();
  const canvasRef = useRef();
  const canvasRef2 = useRef();
  var lastReadyTime = null;
  var intervalId = null;
  var isReady = false;
  const { user, token } = useAuth();
  const serverUrl = process.env.REACT_APP_SERVER_ADDRESS;

  const initialize = () => {
    setProcessedImageUrl(null);
    setProcessedImageUrl(false);
    loadModels();
  };

  const loadModels = async () => {
    setStatus("Initializing..");
    Promise.all([
      faceApi.nets.tinyFaceDetector.loadFromUri(MODEL_URL),
      faceApi.nets.faceLandmark68Net.loadFromUri(MODEL_URL),
      faceApi.nets.faceRecognitionNet.loadFromUri(MODEL_URL),
      faceApi.nets.faceExpressionNet.loadFromUri(MODEL_URL),
    ]).then(startVideoStream);
  };

  const startVideoStream = () => {
    navigator.getUserMedia(
      { video: {} },
      (stream) => {
        videoRef.current.srcObject = stream;
        setIsStreamingVideo(true);
      },
      (err) => console.log(err)
    );
  };

  /** Stops video streams and clears the canvas */
  const stopVideoStream = () => {
    canvasRef.current.getContext("2d").clearRect(0, 0, videoWidth, videoHeight);
    canvasRef2.current
      .getContext("2d")
      .clearRect(0, 0, videoWidth, videoHeight);

    clearInterval(intervalId);
    setIsStreamingVideo(false);

    if (videoRef.current) {
      const stream = videoRef.current.srcObject;
      const tracks = stream.getTracks();
      tracks.forEach(function (track) {
        console.log("stopping tracks", track);
        track.stop();
      });
      videoRef.current.srcObject = null;
    }
  };

  const handleVideoOnPlay = () => {
    intervalId = setInterval(async () => {
      canvasRef.current.innerHTML = faceApi.createCanvasFromMedia(
        videoRef.current
      );
      const displaySize = { width: videoWidth, height: videoHeight };

      faceApi.matchDimensions(videoRef.current, displaySize);
      const detections = await faceApi.detectAllFaces(
        videoRef.current,
        new faceApi.TinyFaceDetectorOptions()
      );

      const resizedDetections = faceApi.resizeResults(detections, displaySize);
      canvasRef.current
        .getContext("2d")
        .clearRect(0, 0, videoWidth, videoHeight);

      faceApi.draw.drawDetections(canvasRef.current, resizedDetections);
      getUserMessage(detections);

      // Take a picture after 2 seconds
      if (isReady && lastReadyTime > READY_TIME) {
        lastReadyTime = 0;
        submit();
      }
    }, TIME_INTERVAL);
  };

  /** Set status message to be displayed to the user */
  const getUserMessage = (detections) => {
    if (detections.length < 1) {
      lastReadyTime = 0;
      isReady = false;
      return setStatus("Align face");
    }

    const { box, imageWidth, imageHeight } = detections[0];

    if (box.right > percentage(imageWidth, 80)) {
      lastReadyTime = 0;
      isReady = false;
      return setStatus("Move left");
    }

    if (box.left < percentage(imageWidth, 20)) {
      lastReadyTime = 0;
      isReady = false;
      return setStatus("Move Right");
    }

    if (box.height < percentage(imageHeight, 40)) {
      lastReadyTime = 0;
      isReady = false;
      return setStatus("Come closer");
    }

    setStatus(STATE_READY);
    lastReadyTime = lastReadyTime + TIME_INTERVAL;
    isReady = true;
  };

  /** Submit captured image for processing */
  async function submit() {
    var ctx = canvasRef2.current.getContext("2d");

    ctx.drawImage(videoRef.current, 0, 0, videoWidth, videoHeight);
    let blob = await new Promise((resolve) =>
      canvasRef2.current.toBlob(resolve, "image/png")
    );
    setIsProcessingImage(true);
    stopVideoStream();
    console.log(user);
    let response = await processMedia(blob, user);
    const json = await response.json();
    console.log(await json);
    if (!response.ok) return alert(json["hydra:description"]);

    const { contentUrl } = json;
    setProcessedImageUrl(serverUrl + contentUrl);
    setIsProcessingImage(false);
    setStatus("Image processed");
  }

  const percentage = (number, percentage) => (number / 100) * percentage;

  return (
    <div className="d-flex h-100 text-center top-margin">
      <header className="mb-auto">
        <nav
          className="navbar navbar-expand-sm navbar-dark fixed-top "
          style={{ backgroundColor: "coral" }}
        >
          <a className="navbar-brand mx-auto" href="/">
            &#x1F355; iPortal Pizza Test Cafe &#x1F355;
          </a>
        </nav>
      </header>

      <main
        role="main"
        className="cover-container d-flex flex-column align-items-center mx-auto justify-content-center"
      >
        {status && (
          <h3 className="alert " role="alert">
            {status}
          </h3>
        )}

        <div className="d-flex justify-content-center align-items-center">
          {isProcessingImage && (
            <div className="d-flex justify-content-center position-absolute ">
              <div className="spinner-border m-5 " role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
          )}

          <video
            ref={videoRef}
            width={videoWidth}
            height={videoHeight}
            autoPlay
            muted
            onPlay={handleVideoOnPlay}
            className="border"
          />
          <canvas
            ref={canvasRef}
            className={"position-absolute"}
            width={videoWidth}
            height={videoHeight}
          />
          <canvas
            ref={canvasRef2}
            className={"position-absolute"}
            width={videoWidth}
            height={videoHeight}
          />
          {processedImageUrl && (
            <img
              src={processedImageUrl}
              className={"position-absolute"}
              width={videoWidth}
              height={videoHeight}
            />
          )}
        </div>

        <button
          disabled={isStreamingVideo}
          className="btn btn-lg btn-primary lead mt-3"
          type="submit"
          onClick={initialize}
        >
          Scan face for secret ingredients &#x1F355;
        </button>
      </main>

      <footer className="footer fixed-bottom py-3 bg-light">
        <div className="container">
          <span className="text-muted">&copy; 2020 iProov</span>
        </div>
      </footer>
    </div>
  );
}

export default Home;
