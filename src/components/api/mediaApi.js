import { useAuth } from "../../contexts/AuthContext";

const processMedia = (blob, user) => {
  const serverUrl = process.env.REACT_APP_SERVER_ADDRESS;
  const mediaEndpoint = serverUrl + "/api/media_objects";

  const formData = new FormData();
  formData.append("file", blob, "file.png");
  formData.append("author", "/api/users/" + user.id);
  try {
    return fetch(mediaEndpoint, {
      method: "POST",
      body: formData,
    });
  } catch (error) {
    console.log(error);
    alert(error);
  }
};

export { processMedia };
