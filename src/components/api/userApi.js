const serverUrl = process.env.REACT_APP_SERVER_ADDRESS;

const login = () => {
  const endpoint = serverUrl + "/authentication_token";
  const data = { email: "henryosim@yahoo.com", password: "password" };
  try {
    return fetch(endpoint, {
      method: "POST",
      // credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  } catch (error) {
    console.log(error);
    alert(error);
  }
};

const getUserData = (userId, token) => {
  const endpoint = serverUrl + "/users/" + userId;
  var bearer = `Bearer ${token}`;
  console.log(bearer);
  try {
    return fetch(endpoint, {
      method: "get",
      credentials: "include",
      headers: {
        Authorization: bearer,
      },
    });
  } catch (error) {
    console.log(error);
    alert(error);
  }
};

export { login, getUserData };
