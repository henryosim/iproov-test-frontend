import React from "react";
import { Navigate, useLocation } from "react-router";
import { useAuth } from "../contexts/AuthContext";

export default function ProtectedRoute({ children }) {
  let { user } = useAuth();
  const currentLocation = useLocation();

  if (!user) {
    return <Navigate to="/login" state={{ from: currentLocation }} />;
  }

  return children;
}
