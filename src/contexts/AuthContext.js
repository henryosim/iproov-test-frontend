import jwtDecode from "jwt-decode";
import { createContext, useContext, useEffect, useState } from "react";
import { login as loginApi } from "../components/api/userApi";
const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({ children }) {
  let [user, setUser] = useState();
  let [isReady, setIsReady] = useState(false);
  let [token, setToken] = useState();

  useEffect(() => {
    setIsReady(true);
  }, []);

  const login = async (email, password) => {
    const response = await loginApi(email, password);
    if (!response.ok) return;
    const { token, id } = await response.json();
    const { username } = jwtDecode(token);
    setUser({ username, id });
    setToken(token);
    return true;
  };

  let value = { user, login, token };

  return (
    <AuthContext.Provider value={value}>
      {isReady && children}
    </AuthContext.Provider>
  );
}
